﻿using System.Security;
using System.Runtime.InteropServices;
using System.Diagnostics.CodeAnalysis;

namespace HelloGL3
{
	public static class WinApi
	{
		[SuppressMessage("Microsoft.Interoperability", "CA1401:PInvokesShouldNotBeVisible")]
		[SuppressMessage("Microsoft.Security", "CA2118:ReviewSuppressUnmanagedCodeSecurityUsage")]
		[SuppressUnmanagedCodeSecurity]
		[DllImport("ntdll.dll", EntryPoint = "NtSetTimerResolution")]
		public static extern void NtSetTimerResolution(uint DesiredResolution, bool SetResolution, ref uint CurrentResolution);
	}
}
