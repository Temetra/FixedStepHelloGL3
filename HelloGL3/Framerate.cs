﻿namespace HelloGL3
{
	public class Framerate
	{
		double previousElapsed = 0.0;
		double currentElapsed = 0.0;

		public void Update(double elapsedTotalSeconds)
		{
			previousElapsed = currentElapsed;
			currentElapsed = elapsedTotalSeconds;
		}

		public double FPS { get { return 1.0 / (currentElapsed - previousElapsed); } }
		public double FrameTime { get { return currentElapsed - previousElapsed; } }
	}
}
