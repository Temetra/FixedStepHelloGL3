﻿using OpenTK;
using OpenTK.Graphics;
using System;

namespace HelloGL3
{
	public class GameWindow : OpenTK.NativeWindow
	{
		private bool disposed = false;
		private bool isClosing = false;
		private IGraphicsContext glContext;

		public GameWindow() : this(
			640, 480, "OpenGL 3 Example",
			3, 0, 
			GraphicsContextFlags.ForwardCompatible | GraphicsContextFlags.Debug,
			GameWindowFlags.Default, GraphicsMode.Default, DisplayDevice.Default)
		{
		}

		public GameWindow(
			int width, int height, string title,
			int glMajorVersion, int glMinorVersion,
			GraphicsContextFlags contextFlags,
			GameWindowFlags options, GraphicsMode mode, DisplayDevice device)
			: base(width, height, title, options, mode, device)
		{
			try
			{
				// Set up context
				glContext = new GraphicsContext(mode, WindowInfo, glMajorVersion, glMinorVersion, contextFlags);
				glContext.MakeCurrent(WindowInfo);
				(glContext as IGraphicsContextInternal).LoadAll();

				// Show window
				Visible = true;
			}
			catch (Exception)
			{
				base.Dispose();
				throw;
			}
		}

		public IGraphicsContext Context
		{
			get { return glContext; }
		}

		public bool IsClosing
		{
			get { return isClosing; }
		}

		protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
		{
			base.OnClosing(e);
			if (!e.Cancel) isClosing = true;
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!disposed)
			{
				if (disposing)
				{
					// Dispose managed resources
				}
			}

			// Dispose unmanaged resources
			if (glContext != null)
			{
				glContext.Dispose();
				glContext = null;
			}

			base.Dispose();
			
			disposed = true;
		}

		public override void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
	}
}
