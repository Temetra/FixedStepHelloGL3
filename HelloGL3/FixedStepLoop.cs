﻿namespace HelloGL3
{
	/// <summary>
	/// Fixed timestep loop
	/// </summary>
	/// <see cref="http://gafferongames.com/game-physics/fix-your-timestep/"/>
	public class FixedStepLoop
	{
		const double maxFrameTime = 0.25;
		double dt;
		double newTime = 0.0;
		double previousTime = 0.0;
		double frameTime = 0.0;
		double accumulator = 0.0;
		System.Diagnostics.Stopwatch timer;

		public FixedStepLoop(double updateHz = 60.0)
		{
			dt = 1.0 / updateHz;
			timer = new System.Diagnostics.Stopwatch();
			timer.Start();
		}

		/// <param name="update">Total elapsed seconds</param>
		/// <param name="draw">Total elapsed seconds, alpha</param>
		public void Execute(System.Action<double> update, System.Action<double, double> draw)
		{
			// Calc accumulator
			newTime = timer.Elapsed.TotalSeconds;
			frameTime = newTime - previousTime;
			previousTime = newTime;
			if (frameTime > maxFrameTime) frameTime = maxFrameTime;
			accumulator += frameTime;

			// Update
			while (accumulator >= dt)
			{
				accumulator -= dt;
				update(newTime);
			}

			// Draw
			// "We can use this remainder value (accumulator) to get a blending factor 
			// between the previous and current physics state simply by dividing by dt. 
			// This gives an alpha value in the range [0,1] which is used to perform 
			// a linear interpolation between the two physics states to get the current 
			// state to render."
			draw(newTime, accumulator / dt);
		}
	}
}
