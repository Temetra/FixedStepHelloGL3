﻿using System;

namespace HelloGL3
{
	static class Program
	{
		[STAThread]
		static void Main()
		{
			// Set minimum timer period
			uint currentTimerRes = 0;
			WinApi.NtSetTimerResolution(9000, true, ref currentTimerRes);

			// Open a new GL context window
			using (GameWindow window = new GameWindow())
			{
				// Load example program
				ExampleProgram example = new ExampleProgram(window);
				example.Load();

				// Start fixed step loop
				window.Context.VSync = false;
				FixedStepLoop loop = new FixedStepLoop(60.0);

				// Run loop
				while(true)
				{
					if (example.ProcessWindowEvents())
					{
						loop.Execute(example.UpdateState, example.Draw);
						window.Title = string.Format("ups: {0:0.0000}, fps: {1:0.0000}", example.UpdateRate.FPS, example.DrawRate.FPS);
						System.Threading.Thread.Sleep(1);
					}
					else
					{
						break;
					}
				}

				// Clean up
				example.Unload();
			}

			// Reset timer period
			WinApi.NtSetTimerResolution(currentTimerRes, true, ref currentTimerRes);
		}
	}
}
